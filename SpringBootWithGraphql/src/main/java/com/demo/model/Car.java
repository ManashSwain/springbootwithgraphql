package com.demo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table
@NoArgsConstructor
public class Car implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private int carId;
	private String carType;
	private String carModel;
	private double price;
	private String fuelType;
	private String color;
	
	public Car(int carId, String carType, String carModel, double price, String fuelType, String color) {
		this.carId = carId;
		this.carType = carType;
		this.carModel = carModel;
		this.price = price;
		this.fuelType = fuelType;
		this.color = color;
	}

}
