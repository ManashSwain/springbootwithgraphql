package com.demo.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.service.CarService;

import graphql.ExecutionResult;

@RestController
public class CarResource {
	
	@Autowired
	private CarService carService;
	

	@PostMapping("/cars")
	public ResponseEntity<Object> getAllCar(@RequestBody String raw) {
		//execute graphql schema
		ExecutionResult result= carService.getGraphql().execute(raw);
		return new ResponseEntity<Object>(result,HttpStatus.OK);
	}

}
