package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.model.Car;
import com.demo.repository.CarRepository;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class GetAllCarDataFatcher implements DataFetcher<List<Car>>{
	
	@Autowired
	private CarRepository carRepository;

	@Override
	public List<Car> get(DataFetchingEnvironment environment) {
		return carRepository.findAll();
	}

}
