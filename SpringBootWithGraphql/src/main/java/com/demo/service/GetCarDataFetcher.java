package com.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.demo.model.Car;
import com.demo.repository.CarRepository;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class GetCarDataFetcher implements DataFetcher<Car> {
	
	@Autowired
	private CarRepository carRepository;

	@Override
	public Car get(DataFetchingEnvironment environment) {
		Car car=carRepository.findById(environment.getArgument("carId")).get();
		return car;
	}

}
