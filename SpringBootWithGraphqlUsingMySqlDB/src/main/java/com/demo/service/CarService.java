package com.demo.service;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.demo.model.Car;
import com.demo.repository.CarRepository;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

@Service("carService")
public class CarService {
	
	@Value("classpath:query.graphql")
	private Resource resource;
	
	@Autowired
	private GetAllCarDataFatcher getAllCarDataFatcher;
	
	@Autowired
	private GetCarDataFetcher getCarDataFetcher;
	
	private GraphQL graphql;
	
	@Autowired
	private CarRepository carRepository;
	
	


	/**
	 * This Method will load the graphql schema
	 * @throws IOException 
	 */
	@PostConstruct
	public void loadGraphQlSchema() throws IOException {
		//loadDataIntoHSQL();
		File schemaFile=null;
		//get schema file
		schemaFile=resource.getFile();
		//parse the schema file using graphql parsers
		TypeDefinitionRegistry typeRegistry=new SchemaParser().parse(schemaFile);
		RuntimeWiring runTimeWiring=buildRunTimeWiring();
		
		GraphQLSchema graphqlSchema=new SchemaGenerator().makeExecutableSchema(typeRegistry, runTimeWiring);
		graphql=GraphQL.newGraphQL(graphqlSchema).build();
		
	}	
	
	private RuntimeWiring buildRunTimeWiring() {
		return RuntimeWiring.newRuntimeWiring().type("Query",typeWiring->
		                                                     typeWiring.dataFetcher("getAllCars", getAllCarDataFatcher)
		                                                                 .dataFetcher("getCar", getCarDataFetcher))
				                                                        .build();
	}
	
	public GraphQL getGraphql() {
		return graphql;
	}
	/**
	 * This Method is used to load data into the HQL data base
	 */
	/*private void loadDataIntoHSQL() {
		Stream.of(
				new Car(101, "SUV","Honda City",250000 , "Disel","Red"),
				new Car(102, "SUV","Dizer Swift",200000 , "peterol","Blue"),
				new Car(103, "SEDAN","Belano",240000 , "Disel","White")
				
				).forEach(car->{
					carRepository.save(car);
				});
	}*/

}
